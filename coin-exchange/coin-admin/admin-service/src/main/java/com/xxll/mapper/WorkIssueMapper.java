package com.xxll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxll.domain.WorkIssue;

public interface WorkIssueMapper extends BaseMapper<WorkIssue> {
}