package com.xxll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxll.domain.SysRolePrivilegeUser;

public interface SysRolePrivilegeUserMapper extends BaseMapper<SysRolePrivilegeUser> {
}