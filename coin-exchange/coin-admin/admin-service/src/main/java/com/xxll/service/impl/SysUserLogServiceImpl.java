package com.xxll.service.impl;

import com.xxll.service.SysUserLogService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxll.domain.SysUserLog;
import com.xxll.mapper.SysUserLogMapper;

@Service
public class SysUserLogServiceImpl extends ServiceImpl<SysUserLogMapper, SysUserLog> implements SysUserLogService {

}
